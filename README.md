# Graphic Equalizer

MATLAB integrated equalizer that allows for attenuation and amplification of multiple frequencies on both left and right audio channels.