function GraphOutputF(handles)
%This function graphs the fft of a sample signal containing the editable
%frequencies in the GUI to show the user the effect of their edits on each
%frequency. It is called by the sliders in the GUI, as well as the "Reset"
%button.
reset=handles.resetbutton.Value;
if reset                           %when the reset button is pressed,
    handles.Lmid1.Value=1;         %every slider is returned to its neutral
    handles.Lmid2.Value=1;         %value.
    handles.Lmid3.Value=1;
    handles.Ltreble1.Value=1;
    handles.Ltreble2.Value=1;
    handles.Ltreble3.Value=1;
    handles.Ltreble4.Value=1;
    handles.Rmid1.Value=1;
    handles.Rmid2.Value=1;
    handles.Rmid3.Value=1;
    handles.Rtreble1.Value=1;
    handles.Rtreble2.Value=1;
    handles.Rtreble3.Value=1;
    handles.Rtreble4.Value=1;    
end
%% frequency bands / parameters
midrange1=250;
midrange2=500;
midrange3=1000;
treble1=2000;
treble2=4000;
treble3=8000;
treble4=16000;


fs=44100;
T=1;
Ts=1/fs;  %Sampling period
fN=fs/2;  %Nyquist frequency is simply the sampling frequency divided by 2
N=T/Ts;   %Number of samples
t=0:Ts:(N-1)*Ts;
f=[midrange1 midrange2 midrange3 treble1 treble2 treble3 treble4];
ft=0:1/T:(N/2-1)/T;

sdelta=.005;                %space between stop band and pass band
pdelta=.001;                %space from frequency to either end of pass band
stop1=midrange1/fN-sdelta;   %coefficients for frequency bands in firpm
pass1=midrange1/fN-pdelta;
pass2=midrange1/fN+pdelta;
stop2=midrange1/fN+sdelta;
stop3=midrange2/fN-sdelta;
pass3=midrange2/fN-pdelta;
pass4=midrange2/fN+pdelta;
stop4=midrange2/fN+sdelta;
stop5=midrange3/fN-sdelta;
pass5=midrange3/fN-pdelta;
pass6=midrange3/fN+pdelta;
stop6=midrange3/fN+sdelta;
stop7=treble1/fN-sdelta;
pass7=treble1/fN-pdelta;
pass8=treble1/fN+pdelta;
stop8=treble1/fN+sdelta;
stop9=treble2/fN-sdelta;
pass9=treble2/fN-pdelta;
pass10=treble2/fN+pdelta;
stop10=treble2/fN+sdelta;
stop11=treble3/fN-sdelta;
pass11=treble3/fN-pdelta;
pass12=treble3/fN+pdelta;
stop12=treble3/fN+sdelta;
stop13=treble4/fN-sdelta;
pass13=treble4/fN-pdelta;
pass14=treble4/fN+pdelta;
stop14=treble4/fN+sdelta;

%% construct sample signal
signal=cos(2*pi*f(1)*t);
for i=2:length(f)              %Sample signal is constructed with the
    signal=signal+cos(2*pi*f(i)*t);  %editable frequencies.
end
L=signal;                 %For this signal, the left and right channels
R=signal;                 %are the same initially.

%% filter and plot

AL=handles.Lmid1.Value;        %The same process as in GraphicEqualizerF
BL=handles.Lmid2.Value;        %is used to filter each frequency in each
CL=handles.Lmid3.Value;        %audio channel.
AR=handles.Rmid1.Value;
BR=handles.Rmid2.Value;
CR=handles.Rmid3.Value;
DL=handles.Ltreble1.Value;
EL=handles.Ltreble2.Value;
FL=handles.Ltreble3.Value;
GL=handles.Ltreble4.Value;
DR=handles.Rtreble1.Value;
ER=handles.Rtreble2.Value;
FR=handles.Rtreble3.Value;
GR=handles.Rtreble4.Value;
b1=[0 stop1 pass1 pass2 stop2 stop3 pass3 pass4 stop4 stop5 pass5 pass6 stop6 stop7 pass7 pass8 stop8 stop9 pass9 pass10 stop10 stop11 pass11 pass12 stop12 stop13 pass13 pass14 stop14 1]; 
bL=[0 0 AL AL 0 0 BL BL 0 0 CL CL 0 0 DL DL 0 0 EL EL 0 0 FL FL 0 0 GL GL 0 0];
bR=[0 0 AR AR 0 0 BR BR 0 0 CR CR 0 0 DR DR 0 0 ER ER 0 0 FR FR 0 0 GR GR 0 0];
fLeft=firpm(350,b1,bL);   %left FIR filter
fRight=firpm(350,b1,bR);  %right FIR filter
L=filter(fLeft,1,L);      %left channel output
R=filter(fRight,1,R);     %right channel output



specL=fft(L);                   %The fft's of both audio channels are
specR=fft(R);                   %constructed, and are plotted on two 
magL=abs(specL(1:N/2));         %different graphs, because the frequencies'
magR=abs(specR(1:N/2));         %spacing is different for midrange and
plot(handles.midaxes,ft,magL,'r-',ft,magR,'b:') %treble frequency bands.
legend(handles.midaxes,'Left Channel','Right Channel')
axis(handles.midaxes,[200 1100 0 5e4])
set(handles.midaxes,'YTickLabel',[])
plot(handles.trebleaxes,ft,magL,'r-',ft,magR,'b:')
axis(handles.trebleaxes,[1500 17000 0 5e4])
set(handles.trebleaxes,'YTickLabel',[])
