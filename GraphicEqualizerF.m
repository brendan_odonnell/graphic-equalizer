function GraphicEqualizerF(handles)
%This function serves to pass the selected audio clip through all
%of the filters determined by the sliders on the GUI. It is called by
%all buttons in the GUI except for the "Reset" button.
%% frequency bands / parameters
midrange1=250;
midrange2=500;
midrange3=1000;                    %frequencies that will be edited
treble1=2000;                      %by the user in the GUI.
treble2=4000;
treble3=8000;
treble4=16000;

fs=44100;                         %sampling rate is assumed to be 44100 Hz
x=getappdata(handles.loadbutton,'x');
L=x(:,1);                          %Stereo signal is split into left and 
R=x(:,2);                          %right channels.
play=handles.pushbutton3.Value;  %pushbutton plays selected audio file
if play
    sound(x,fs)
end
fN=fs/2;         %Nyquist frequency
sdelta=.004;         %length of transition band
pdelta1=.0015;          %space from frequency to either end of pass band,
pdelta2=.007;           %varies as the frequencies increase as they aren't
pdelta3=.025;           %evenly spaced. 
pdelta4=.055;
pdelta5=.115;
pdelta6=.220;

pass1=midrange1/fN-.005;  %coefficients for frequency bands in firpm
stop1=pass1-sdelta;
pass2=midrange1/fN+pdelta1;
stop2=pass2+sdelta;
pass3=midrange2/fN-pdelta1;
stop3=pass3-sdelta;
pass4=midrange2/fN+pdelta1;
stop4=pass4+sdelta;
pass5=midrange3/fN-pdelta2;
stop5=pass5-sdelta;
pass6=midrange3/fN+pdelta2;
stop6=pass6+sdelta;
pass7=treble1/fN-pdelta3;
stop7=pass7-sdelta;
pass8=treble1/fN+pdelta3;
stop8=pass8+sdelta;
pass9=treble2/fN-pdelta4;
stop9=pass9-sdelta;
pass10=treble2/fN+pdelta4;
stop10=pass10+sdelta;
pass11=treble3/fN-pdelta5;
stop11=pass11-sdelta;
pass12=treble3/fN+pdelta5;
stop12=pass12+sdelta;
pass13=treble4/fN-pdelta6;
stop13=pass13-sdelta;
pass14=treble4/fN+pdelta6;
stop14=pass14+sdelta;
%% filters
AL=handles.Lmid1.Value;               %Values for firpm filter magnitudes
BL=handles.Lmid2.Value;               %are all linked to the GUI sliders
CL=handles.Lmid3.Value;
AR=handles.Rmid1.Value;
BR=handles.Rmid2.Value;
CR=handles.Rmid3.Value;
DL=handles.Ltreble1.Value;            
EL=handles.Ltreble2.Value;            
FL=handles.Ltreble3.Value;            
GL=handles.Ltreble4.Value;            
DR=handles.Rtreble1.Value;            
ER=handles.Rtreble2.Value;            
FR=handles.Rtreble3.Value;
GR=handles.Rtreble4.Value;
b1=[0 stop1 pass1 pass2 stop2 stop3 pass3 pass4 stop4 stop5 pass5 pass6 stop6 stop7 pass7 pass8 stop8 stop9 pass9 pass10 stop10 stop11 pass11 pass12 stop12 stop13 pass13 pass14 stop14 1]; 
bL=[0 0 AL AL 0 0 BL BL 0 0 CL CL 0 0 DL DL 0 0 EL EL 0 0 FL FL 0 0 GL GL 0 0];
bR=[0 0 AR AR 0 0 BR BR 0 0 CR CR 0 0 DR DR 0 0 ER ER 0 0 FR FR 0 0 GR GR 0 0];
fLeft=firpm(350,b1,bL);   %left FIR filter
fRight=firpm(350,b1,bR);  %right FIR filter
L=filter(fLeft,1,L);      %left channel output
R=filter(fRight,1,R);     %right channel output
y=[L R];                  %Signal output concatenated into stereo





PlayLeft=handles.PlayLeft.Value;       %Left channel is played when
if PlayLeft                            %when button is pushed
    sound(L,fs)    
end

PlayRight=handles.PlayRight.Value;     %Right channel is played when
if PlayRight                           %button is pushed
    sound(R,fs)
end

PlayBoth=handles.PlayBoth.Value;       %Stereo sound is played when
if PlayBoth                            %button is pushed
    sound(y,fs)
end

Save=handles.SaveButton.Value;         %Edited audio is saved when button
if Save                                %is pushed
    string=handles.audiofileText.String;  %User defined filename in 
    filetype='.wav'                       %GUI editable text box
    name=[string filetype]
    audiowrite(name,y,fs);
    disp('Your file has been saved.')
end









